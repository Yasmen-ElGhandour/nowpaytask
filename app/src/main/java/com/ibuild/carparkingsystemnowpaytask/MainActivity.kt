package com.ibuild.carparkingsystemnowpaytask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.ibuild.carparkingsystemnowpaytask.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    var Nlots = 5
    var parckingVehicleslist = ArrayList<Vehicle>(Nlots)

    var duration: Int = 0
    var cost: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        NewBooking()         //clicked new booking Btn
        Exit()         //clicked exit Btn
    }

    private fun Exit() {
        binding.ExitBtn.setOnClickListener {
            var vehicleName = getVehicleName()
            var EntryTime = getEntryTime()
            var ExitTime = getExitTime()

            exit(Vehicle(vehicleName), EntryTime, ExitTime)

        }
    }

    fun getVehicleName() : String{
        return binding.vehicleNameEdt.text.toString()
    }

    fun getEntryTime() : Int{

        var EntryTime = binding.vehicleEntryTimeEdt.text.toString().toInt()
        if (EntryTime <= 24)
        return EntryTime
        else
        binding.vehicleEntryTimeEdt.setError("Time must between 0 ..24");

        return  0
    }

    fun getExitTime() : Int{

        var ExitTime = binding.vehicleExitTimeEdt.text.toString().toInt()
        if (ExitTime <= 24)
            return ExitTime
        else
            binding.vehicleExitTimeEdt.setError("Time must between 0 ..24");

        return  0
    }

    fun clearCostTV(){
        binding.costTV.setText("")
    }

    fun setCostTv(parkingFee : String){
        binding.costTV.setText(parkingFee)

    }

    fun NewBooking() {

        binding.NewbookingBtn.setOnClickListener {
            clearCostTV()
            var vehicleName = getVehicleName()
            var EntryTime = getEntryTime()

            if(parckingVehicleslist.size >= Nlots){
                Toast.makeText(applicationContext, "Sorry No Empty Area Available For Now", Toast.LENGTH_LONG).show()

            }else{
                enter(Vehicle(vehicleName), EntryTime)
            }


        }
    }

    fun calcPCost(EntryTime: Int, Exittime: Int): Int {
        var Entertime = EntryTime

        when  {

            EntryTime == 24 -> Entertime = 0                //case 00:00
            //..............Enter before 6 Am ..........................
                //cas1
            Entertime <= 6 && Exittime in 6..10 ->{
                duration = Exittime - 6
                cost = 3 * duration
            }
            //cas2
            Entertime <= 6 && Exittime in 6..24 ->{
                duration = Exittime - 6
                if (duration > 4) {
                    cost = 3 * 4 + (duration - 4) * 1
                }
            }
            //case3
            Entertime <= 6 && Exittime <= 6 ->{
                duration = Exittime - Entertime
                cost = 0 * duration
            }
            //..............Enter From 6 to 10 ...........................
            //cas1
            Entertime in 6..10 && Exittime in 6..10 ->{
                duration = Exittime - Entertime
                cost = 3 * duration
            }
            //case2
            Entertime in 6..10 && Exittime in 10..24 -> {
                var duration1 = 10 - Entertime
                var duration2 = Exittime - 10
                cost = (3 * duration1) + (duration2 * 1)
            }
            //case3
            Entertime in 6..10 && Exittime <= 6 -> {
                var duration1 = 10 - Entertime
                var duration2 = 24 - 10
                cost = (3 * duration1) + (duration2 * 1)
            }
            //.................Enter From 10 to 24 .........................
            //cas1
            Entertime in 10..24 && Exittime in 10..24 ->{
                duration = Exittime - Entertime
                cost = duration * 1
            }
            //cas2
            Entertime in 10..24 && Exittime <= 6 ->{
                duration = 24 - Entertime
                cost = duration * 1
            }
            //case3
            Entertime in 10..24 && Exittime in 6..10 ->{
                var duration1 = 24 - Entertime
                var duration2 = Exittime - 6

                cost = duration1 * 1 + duration2 * 3
            }


        }
        return cost
    }

    fun exit(vehicle: Vehicle, entryTime: Int, exitTime: Int): Int {

        if (!parckingVehicleslist.isEmpty()) {
            parckingVehicleslist.remove(vehicle)
            var parkingFee = calcPCost(entryTime, exitTime)
            setCostTv(parkingFee.toString())

            return parkingFee

        } else {
            Toast.makeText(applicationContext, "Sorry No Vehicle Have This Name", Toast.LENGTH_LONG).show()

            return 0

        }

    }

    fun enter(vehicle: Vehicle, entryTime: Int): Boolean {
        //store vehicle
        if (parckingVehicleslist.size < Nlots) {

            parckingVehicleslist.add(vehicle)
            Toast.makeText(
                applicationContext,
                "Your vehicle Parking Successfully",
                Toast.LENGTH_LONG
            ).show()
            return true

        }
        return false

    }
}